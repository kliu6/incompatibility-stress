# Incompatibility stress

The intrinsic anisotropy of the crystal leads to an additional stress field near the grain boundary. Here provides a python module to conveniently evaluate the incompatibility stress for given materials and grain orientations. 

# Requirement
- numpy
- random

# License
MIT License

