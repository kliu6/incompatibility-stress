import numpy
import random

def generator():
    u = random.random()
    v = random.random()
    s = 2*u-1
    t = 2*v-1
    r2 = s*s+t*t
    while r2 > 1:
        u = random.random()
        v = random.random()
        s = 2*u-1
        t = 2*v-1
        r2 = s*s+t*t
    w = [2*s*numpy.sqrt(1-r2),2*t*numpy.sqrt(1-r2),1-2*r2]
    w = w / numpy.linalg.norm(w)
    return w

# This function is to calculate IS by solving 24*24 matrix.
## input: external stress Sigma: 6*1 array
## c11, c12, c44
## rotation matrix 1&2: 3*3 narray
def MismatchStress_D(Sigma,c11,c12,c44,RM1,RM2):
    Norm1 = numpy.linalg.det(RM1)
    Norm2 = numpy.linalg.det(RM2)
    if round(Norm1) != 1 or round(Norm2) != 1:
        print('Check input rotation matrix!')
        exit()
    Elastic = Tensor(c11,c12,c44)
    Compliance = Reverse(numpy.linalg.pinv(Transform9(Elastic)))
    S1 = RotateComplianceTensor(Compliance,RM1)
    C1 = numpy.linalg.inv(S1)
    S2 = RotateComplianceTensor(Compliance,RM2)
    C2 = numpy.linalg.inv(S2)
    M = numpy.zeros((24,24))
    M[0:6,0:6] = C1
    M[6:12,6:12] = C2
    left = numpy.ones((12,12))*-1
    Left = numpy.diag(numpy.diag(left))
    M[0:12,12:] = Left
    # Compliance tensor * Strain - Stress = 0
    M[12,0] = 1
    M[12,6] = -1
    M[13,2] = 1
    M[13,8] = -1
    M[14,4] = 1
    M[14,10] = -1
    # strain 11, 33, 13 are equal for the two grains
    M[15,13] = 1
    M[16,19] = 1
    M[17,15] = 1
    M[18,21] = 1
    M[19,17] = 1
    M[20,23] = 1
    # Sigma_22, 23, 21 are known
    M[21,12] = 1
    M[21,18] = 1
    M[22,14] = 1
    M[22,20] = 1
    M[23,16] = 1
    M[23,22] = 1
    # sum of Sigma_11, 33, 13 = 0
    N = numpy.zeros((24,1))
    N[15,0] = Sigma[1]
    N[16,0] = Sigma[1]
    N[17,0] = Sigma[3]
    N[18,0] = Sigma[3]
    N[19,0] = Sigma[5]
    N[20,0] = Sigma[5]
    N[21,0] = Sigma[0]*2
    N[22,0] = Sigma[2]*2
    N[23,0] = Sigma[4]*2
    Solution = numpy.dot(numpy.linalg.pinv(M),N)
    INC = Solution[12:17] - Solution[18:23]
    return INC
## This function is to calculate the mismatch stress (6 components) for two grains with
## given rotation matrix and elastic property.
## input: external stress Sigma: 6*1 array
## c11, c12, c44
## rotation matrix 1&2: 3*3 narray
def MismatchStress(Sigma,c11,c12,c44,RM1,RM2):
    Norm1 = numpy.linalg.det(RM1)
    Norm2 = numpy.linalg.det(RM2)
    if round(Norm1) != 1 or round(Norm2) != 1:
        print('Check input rotation matrix!')
        exit()
    Elastic = Tensor(c11,c12,c44)
    Compliance = Reverse(numpy.linalg.pinv(Transform9(Elastic)))
    S1 = RotateComplianceTensor(Compliance,RM1)
    S2 = RotateComplianceTensor(Compliance,RM2)
    S_star = S2 - S1
    E_star = numpy.dot(S_star,Sigma)
    S_mix = 0.5 * S1 + 0.5 * S2
    G = numpy.zeros((6,6))
    D = S_mix[0,0]*S_mix[2,4]*S_mix[2,4]+S_mix[2,2]*S_mix[0,4]*S_mix[0,4]+S_mix[4,4]*S_mix[0,2]*S_mix[0,2]-\
        S_mix[0,0]*S_mix[2,2]*S_mix[4,4]-2*S_mix[0,2]*S_mix[0,4]*S_mix[2,4]
    G[0,0] = (S_mix[2,2]*S_mix[4,4]-S_mix[2,4]*S_mix[2,4])/D
    G[0,2] = (S_mix[0,4]*S_mix[2,4]-S_mix[0,2]*S_mix[4,4])/D
    G[2,0] = G[0,2]
    G[2,2] = (S_mix[0,0]*S_mix[4,4]-S_mix[0,4]*S_mix[0,4])/D
    G[0,4] = (S_mix[0,2]*S_mix[2,4]-S_mix[0,4]*S_mix[2,2])/D
    G[4,0] = G[0,4]
    G[4,4] = (S_mix[0,0]*S_mix[2,2]-S_mix[0,2]*S_mix[0,2])/D
    G[2,4] = (S_mix[0,2]*S_mix[0,4]-S_mix[2,4]*S_mix[0,0])/D
    G[4,2] = G[2,4]
    Sigma_inc = numpy.dot(G,E_star)
    return Sigma_inc

## Reverse 9*9 matrix to Tensor
def Reverse(Matrix):
    reverse = numpy.zeros((3, 3, 3, 3))
    if numpy.size(Matrix) == 36:
        MatrixBig = numpy.zeros((9, 9))
        MatrixBig[0:6, 0:6] = Matrix
        MatrixBig[0:6, 6:9] = Matrix[0:6, 3:6]
        MatrixBig[6:9, 0:9] = MatrixBig[3:6, 0:9]
        Matrix = MatrixBig

    if numpy.size(Matrix) == 81:
        N = [[0, 0], [1, 1], [2, 2], [0, 1], [1, 2], [0, 2], [1, 0], [2, 1], [2, 0]]
        for i in range(0, 9):
            for j in range(0, 9):
                reverse[N[i][0], N[i][1], N[j][0], N[j][1]] = Matrix[i, j]
        return reverse


def RM_generator():
    v1 = generator()
    v2 = generator()
    v2 = v2 - v1 * numpy.dot(v1,v2)
    v2 = v2 / numpy.linalg.norm(v2)
    v3 = numpy.cross(v1,v2)
    RM = [v1,v2,v3]
    RM = numpy.asarray(RM)
    return RM

## generate random rotation matrix (which are also base vectors for random oriented grain)

## This function is to rotate the 4th order **Compliance** tensor with rotation matrix.
## Input: input tensor(4th order), rotation matrix(3*3).
## Output:tensor after rotation with Voigt notation(6*6).
def RotateComplianceTensor(Tensor, Matrix):
    T = Matrix
    Rotated = numpy.zeros((3, 3, 3, 3), dtype=float)
    for k in range(0, 3):
        for l in range(0, 3):
            for s in range(0, 3):
                for t in range(0, 3):
                    Rotated[k, l, s, t] = 0
                    for m in range(0, 3):
                        for n in range(0, 3):
                            for p in range(0, 3):
                                for r in range(0, 3):
                                    Rotated[k, l, s, t] += Tensor[m, n, p, r] * T[k, m] * T[l, n] * T[s, p] * T[t, r]

    Voigt6 = Transform6(Rotated)
    Multiplier1 = numpy.ones((3, 3))
    Multiplier2 = 2 * numpy.ones((3, 3))
    Multiplier4 = 4 * numpy.ones((3, 3))
    Top = numpy.hstack((Multiplier1, Multiplier2))
    Bottom = numpy.hstack((Multiplier2, Multiplier4))
    Multiplier = numpy.vstack((Top, Bottom))
    Output = numpy.multiply(Voigt6, Multiplier)
    return Output
## This function is to build a fourth-order tensor with given elastic components.
## Input�? c11, c12, c44, Flag
## Output: 4-order Tensor by default;
##         9*9 matrix with Voigt notation (11,12,13,21,22,23,31,32,33) when Flag=9;
##         6*6 matrix with Voigt notation (11,22,33,32,31,21) when Flag = 6.
def Tensor(c11,c12,c44,Flag=0):
    a = numpy.zeros((3,3,3,3),dtype = float)
    a[1,1,1,1]=a[2,2,2,2]=a[0,0,0,0]=c11
    a[1,1,2,2]=a[1,1,0,0]=a[2,2,1,1]=a[0,0,1,1]=a[2,2,0,0]=a[0,0,2,2]=c12
    a[1,2,1,2]=a[1,0,1,0]=a[2,0,2,0]=a[2,1,2,1]=a[0,1,0,1]=a[0,2,0,2]= \
    a[1,2,2,1]=a[1,0,0,1]=a[2,0,0,2]=a[2,1,1,2]=a[0,1,1,0]=a[0,2,2,0]=c44
    if Flag == 9:
        Output = Transform9(a)
    elif Flag == 6:
        Output = Transform6(a)
    else:
        Output = a
    return Output

## This function is to transform a 4th-ordered tensor into Voigt notation.
## This function is for 6*6
def Transform6(tensor):
    Dim = tensor.ndim
    N=[[0,0],[1,1],[2,2],[1,2],[2,0],[0,1]]
    Voigt6 = numpy.zeros((6,6))
    if Dim == 4:
        for i in range(0,6):
            for j in range(0,6):
                Voigt6[i,j]=tensor[N[i][0],N[i][1],N[j][0],N[j][1]]
        return Voigt6
    else:
        print('Incorrect tensor')
        exit()


## This function is to transform a 4th-ordered tensor into Voigt notation.
## This function is for 9*9
def Transform9(tensor):
    Dim = tensor.ndim
    N=[[0,0],[1,1],[2,2],[0,1],[1,2],[0,2],[1,0],[2,1],[2,0]]
    Voigt9 = numpy.zeros((9,9))
    if Dim == 4:
        for i in range(0,9):
            for j in range(0,9):
                Voigt9[i,j]=tensor[N[i][0],N[i][1],N[j][0],N[j][1]]
        return Voigt9
    else:
        print('Incorrect tensor')
        exit()

## This function is to calculate the Young's modulus along the given vector.
## Input: Vector, 1*3
##        Tensor, 6*6
##        Type of Tensor, "C" for stiffness tensor and "S" for compliance tensor
## Output: Young's modulus, has the same unit as input stiffness tensor
def YoungsModulus(Vector,Tensor,Type):
    if numpy.size(Tensor) != 36:
        print('Check input tensor!')
        exit()
    if Type == 'C':
        Tensor_S = numpy.linalg.pinv(Transform9(Reverse(Tensor)))
        Tensor = Tensor_S[0:6,0:6]
        Multiplier1 = numpy.ones((3,3))
        Multiplier2 = 2*numpy.ones((3,3))
        Multiplier4 = 4*numpy.ones((3,3))
        Top = numpy.hstack((Multiplier1,Multiplier2))
        Bottom = numpy.hstack((Multiplier2,Multiplier4))
        Multiplier = numpy.vstack((Top,Bottom))
        Tensor = numpy.multiply(Tensor,Multiplier)
    S = Tensor
    V = Vector / numpy.linalg.norm(Vector)
    L0, L1, L2 = V[0], V[1], V[2]
    ReciE = S[0,0]*L0**4+S[1,1]*L1**4+S[2,2]*L2**4+ \
    (S[3,3]+2*S[1,2])*(L1**2*L2**2)+(S[4,4]+2*S[0,2])*(L0**2*L2**2)+(S[5,5]+2*S[0,1])*(L0**2*L1**2)+ \
    2*((S[0,3]+S[4,5])*L0**2+S[1,3]*L1**2+S[2,3]*L2**2)*L1*L2+ \
    2*((S[1,4]+S[3,5])*L1**2+S[0,4]*L0**2+S[2,4]*L2**2)*L0*L2+ \
    2*((S[2,5]+S[3,4])*L2**2+S[0,5]*L0**2+S[1,5]*L1**2)*L0*L1
    Youngs = 1/ReciE
    return Youngs
    
    
## This function is to calculate the incompatibility stress directly from compliance tensor with two methods.
## Input: Sigma: external stress, 6*1
##        S1:  compliance tensor 1
##        S2:  compliance tensor 2
##        Type: method 1 or method 2
def MismatchStress_Comp(Sigma,S1,S2,Type = 1):
    if Type == 1:
        S_star = S2 - S1
        E_star = numpy.dot(S_star,Sigma)
        S_mix = 0.5 * S1 + 0.5 * S2
        G = numpy.zeros((6,6))
        D = S_mix[0,0]*S_mix[2,4]*S_mix[2,4]+S_mix[2,2]*S_mix[0,4]*S_mix[0,4]+S_mix[4,4]*S_mix[0,2]*S_mix[0,2]-\
            S_mix[0,0]*S_mix[2,2]*S_mix[4,4]-2*S_mix[0,2]*S_mix[0,4]*S_mix[2,4]
        G[0,0] = (S_mix[2,2]*S_mix[4,4]-S_mix[2,4]*S_mix[2,4])/D 
        G[0,2] = (S_mix[0,4]*S_mix[2,4]-S_mix[0,2]*S_mix[4,4])/D 
        G[2,0] = G[0,2] 
        G[2,2] = (S_mix[0,0]*S_mix[4,4]-S_mix[0,4]*S_mix[0,4])/D 
        G[0,4] = (S_mix[0,2]*S_mix[2,4]-S_mix[0,4]*S_mix[2,2])/D 
        G[4,0] = G[0,4] 
        G[4,4] = (S_mix[0,0]*S_mix[2,2]-S_mix[0,2]*S_mix[0,2])/D 
        G[2,4] = (S_mix[0,2]*S_mix[0,4]-S_mix[2,4]*S_mix[0,0])/D 
        G[4,2] = G[2,4]
        Sigma_inc = numpy.dot(G,E_star)
        return Sigma_inc
    elif Type == 2:
        C1 = numpy.linalg.inv(S1)
        C2 = numpy.linalg.inv(S2)
        M = numpy.zeros((24,24))
        M[0:6,0:6] = C1
        M[6:12,6:12] = C2
        left = numpy.ones((12,12))*-1
        Left = numpy.diag(numpy.diag(left))
        M[0:12,12:] = Left
        # Compliance tensor * Strain - Stress = 0 
        M[12,0] = 1
        M[12,6] = -1
        M[13,2] = 1
        M[13,8] = -1
        M[14,4] = 1
        M[14,10] = -1
        # strain 11, 33, 13 are equal for the two grains
        M[15,13] = 1
        M[16,19] = 1
        M[17,15] = 1
        M[18,21] = 1
        M[19,17] = 1
        M[20,23] = 1
        # Sigma_22, 23, 21 are known 
        M[21,12] = 1
        M[21,18] = 1
        M[22,14] = 1
        M[22,20] = 1
        M[23,16] = 1
        M[23,22] = 1
        # sum of Sigma_11, 33, 13 = 0  
        N = numpy.zeros((24,1))
        N[15,0] = Sigma[1]
        N[16,0] = Sigma[1]
        N[17,0] = Sigma[3]
        N[18,0] = Sigma[3]
        N[19,0] = Sigma[5]
        N[20,0] = Sigma[5]
        N[21,0] = Sigma[0]*2
        N[22,0] = Sigma[2]*2
        N[23,0] = Sigma[4]*2
        Solution = numpy.dot(numpy.linalg.pinv(M),N)
        INC = Solution[18:] - Solution[12:18]
        return INC
    else: 
        print('Invalid!')
        exit()
        
        
### This function translate stress state into von Mises Stress         
def Mises(S):
    M = (0.5 * ((S[0]-S[1])**2 + (S[0]-S[2])**2 + (S[2]-S[1])**2 + 6 * (S[3]**2 + S[4]**2 + S[5]**2)))**0.5
    return M
    
### This function is to build matrices for directly solving IS.
def BuildMat():
    M = numpy.zeros((24,24))
    left = numpy.ones((12,12))*-1
    Left = numpy.diag(numpy.diag(left))
    M[0:12,12:] = Left
    # Compliance tensor * Strain - Stress = 0 
    M[12,0] = 1
    M[12,6] = -1
    M[13,2] = 1
    M[13,8] = -1
    M[14,4] = 1
    M[14,10] = -1
    # strain 11, 33, 13 are equal for the two grains
    M[15,13] = 1
    M[16,19] = 1
    M[17,15] = 1
    M[18,21] = 1
    M[19,17] = 1
    M[20,23] = 1
    # Sigma_22, 23, 21 are known 
    M[21,12] = 1
    M[21,18] = 1
    M[22,14] = 1
    M[22,20] = 1
    M[23,16] = 1
    M[23,22] = 1
    # sum of Sigma_11, 33, 13 = 0  
    N = numpy.zeros((24,1))
    return M, N
    
    
def MismatchStress_DM(S1,S2,Sigma,M,N,Type = 'S'):
    if Type == 'S':
        C1 = numpy.linalg.inv(S1)
        C2 = numpy.linalg.inv(S2)
    elif Type == 'C':
        C1 = S1
        C2 = S2
    M[0:6,0:6] = C1
    M[6:12,6:12] = C2
    N[15,0] = Sigma[1]
    N[16,0] = Sigma[1]
    N[17,0] = Sigma[3]
    N[18,0] = Sigma[3]
    N[19,0] = Sigma[5]
    N[20,0] = Sigma[5]
    N[21,0] = Sigma[0]*2
    N[22,0] = Sigma[2]*2
    N[23,0] = Sigma[4]*2
    Solution = numpy.dot(numpy.linalg.pinv(M),N)
    INC = Solution[12:17] - Solution[18:23]
    return INC
